### Problem Statement 17

You are given a dictionary D of N words. (The length of each word is at least two letters.) Given a string of K, not necessarily distinct letters, find all possible words that can be formed using the K letters. You can use each letter only once.


**Input Format**


The first line of input consists of an integer T that represents the number of test cases. Then T lines follow with the input format as follows:
N W<sub>0</sub> W<sub>1</sub> � W<sub>N-1</sub> K


Here N is the number of words in the dictionary D. Then W<sub>0</sub>, W<sub>1</sub>, �, W<sub>N-1</sub> are the N words in the dictionary. K is the string of letters.


**Output Format**


For each test case, output all the words that are valid dictionary words with respect to the dictionary D that can be formed given the letters of K. The words should be output in alphabetical order. Assuming there are J words, the words should be output as follows:


G<sub>0</sub> G<sub>1</sub> � G<sub>J-1</sub>


Here, all the G<sub>i</sub>s are valid words as defined above, and in alphabetical order.

**Note: Ignore case.**


**Constraints**


For T and N, the constraints are as follows:

1<=T<=10000

1<=N<=10000

Let L<sub>i</sub> be the length of the word W<sub>i</sub> in the dictionary where i ranges from 0 to N - 1. 

Let E be the length of the string K

2 <= L<sub>i</sub> <= 10

2 <= E <= 15


**Sample Input**
```
1
5 ab ad bad rad fad abracadabra
```
**Sample Output**
```
ab ad bad rad
```