tc =int(raw_input())

while tc:
	l=raw_input()
	N,M = [int(x) for x in l.split()]
	mat=[]
	dp=[]
	for x in range(N):
		temp=raw_input()
		dp.append([0]*M)
		mat.append([int(x) for x in temp.split()])
	for i in range(N):
		for j in range(M):
			maxV=dp[i][j]
			if (j-1)>=0 and dp[i][j-1]>maxV:
				maxV = dp[i][j-1]
			if (i-1)>=0 and dp[i-1][j]>maxV:
				maxV = dp[i-1][j]
			dp[i][j] = mat[i][j]+maxV
	print dp[N-1][M-1]
	tc = tc -1
