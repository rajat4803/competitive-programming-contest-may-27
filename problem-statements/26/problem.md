The DNA code consists of four nucleobases Adenine, Guanine, Thymine and Cytosine: A, G, T, C for short. A DNA molecule consists of two strands, each strand containing these nucleobases, in a double helix structure. In the structure, each type of nucleobase on one strand pairs up with just one type of nucleobase in the other. A pairs T and G pairs with C.

Given a strand with AGCTCAG, this strand pairs with a strand consisting of the nucleobases TCGAGTC.

So, given a DNA strand, you are to output what the corresponding strand would be as outlined above..

**Input Format:**

The first line of input consists of the integer T which represents the number of test cases. Then T lines follow consisting of a string S. The string can only contain the letters A, G, C or T. The length of the string is 10^9 characters.

**Output Format:**

For each test case, output the corresponding strand would be (as defined in the description above.)

**Constraints**

1 <= T <= 1000

1 <= len(S) <= 10^9

**Sample Input**

```
2
GCATTCGT
CGTACCGGGGTCA
```
**Sample Output**
```
CGTAAGCA
GCATGGCCCCAGT
```